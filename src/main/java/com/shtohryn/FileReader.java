package com.shtohryn;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReader implements Reader {
    final static Logger LOGGER = Logger.getLogger(FileReader.class);

    public String read() {
        String message = "";
        try {
            URI url = ClassLoader.getSystemResource("message.txt").toURI();
            byte[] fileBytes = Files.readAllBytes(Paths.get(url));
            LOGGER.info("Bite array from file have been gotten ");
            message = new String(fileBytes);
            LOGGER.info("Bite array from file have been gotten ");
        } catch (URISyntaxException e) {
            LOGGER.error(e);
        } catch (NullPointerException e) {
            LOGGER.error(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (IOException e) {
//            LOGGER.error(e);
//        }
        return message;
    }
}
