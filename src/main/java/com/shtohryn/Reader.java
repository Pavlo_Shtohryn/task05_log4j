package com.shtohryn;

public interface Reader {
    String read();
}
