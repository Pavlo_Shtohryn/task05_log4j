package com.shtohryn;

public class Main {
    public static void main(String[] args) {
        Reader reader = new FileReader();
        String message = reader.read();
        System.out.println(message);
    }
}
